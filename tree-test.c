#include <stdio.h>
#include "tree.h"


tree *
add(tree * t, const char *label) {
  pub_key pub;
  priv_key priv;
  fake_rand (label, sizeof(priv.data), priv.data);
  pub = priv_key_get_pub_key (&priv);
  return tree_add_member (t, label, &pub, NULL);
}

int main() {
  tree *t = NULL;

  t = add(t, "Alice");
  t = add(t, "Bob");
  t = add(t, "Charlie");

  fprint_tree (stdout, t);
  
  tree_free (t);
  return 0;
}
