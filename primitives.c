#include <stdbool.h>
#include <string.h>
#include <nettle/sha2.h>
#include <nettle/hmac.h>
#include <nettle/base64.h>
#include <sys/random.h>
#include "primitives.h"

void
fprint_buf (FILE *f, const char* label, const uint8_t *data) {
  struct base64_encode_ctx ctx;
  size_t len;
  char out[BASE64_ENCODE_LENGTH(CURVE25519_SIZE)];
  base64_encode_init (&ctx);
  len = base64_encode_update (&ctx, out, CURVE25519_SIZE, data);
  len += base64_encode_final (&ctx, out + len);
  if (label)
    fprintf (f, "%5s: ", label);
  fwrite (out, len, 1, f);
}

void
fprint_pub_key (FILE* f, const char * label, const pub_key *pub) {
  if (label)
    fprintf(f, "%10s ", label);
  fprint_buf (f, label ? "pub" : NULL, pub->data);
}

void
fprint_priv_key (FILE* f, const char * label, const priv_key *priv) {
  fprintf(f, "%10s ", label);
  fprint_buf (f, "priv", priv->data);
}

void
fprint_sym_key (FILE* f, const char * label, const sym_key *sym) {
  fprintf(f, "%10s ", label);
  fprint_buf (f, "sym", sym->data);
}

pub_key
priv_key_get_pub_key (const priv_key *priv) {
  pub_key ret;
  curve25519_mul_g (ret.data, priv->data);
  return ret;
}

sym_key
kx (const priv_key *priv, const pub_key* pub) {
  sym_key ret;
  curve25519_mul (ret.data, priv->data, pub->data);
  return ret;
}

/* this "iota" function should match what is happening in richard barnes' code */ 
priv_key
sym_key_derive_priv_key (const sym_key *sym) {
  priv_key ret;
  struct hmac_sha256_ctx ctx;
  hmac_sha256_set_key (&ctx, sizeof (sym->data), sym->data);
  hmac_sha256_update (&ctx, 4, (uint8_t*)"iota");
  hmac_sha256_digest (&ctx, sizeof (ret.data), ret.data);
  ret.data[0] &= 248;
  ret.data[31] &= 127;
  ret.data[31] |= 64;

  return ret;
}
sym_key
sym_key_ratchet (const sym_key *sym) {
  sym_key ret;
  struct sha256_ctx ctx;
  sha256_init (&ctx);
  
  sha256_update (&ctx, sizeof (sym->data), sym->data);
  sha256_digest (&ctx, sizeof (ret.data), ret.data);
  
  return ret;
}

struct _rnd_st {
  struct sha256_ctx ctx;
  bool init;
  uint8_t state[32];
  const char *seed;
};


static struct _rnd_st _priv_key = { .seed = "Hello, World!" };
static struct _rnd_st _sym_key = { .seed = "Good Bye!" };

void
dumb_32_bytes (struct _rnd_st *st, uint8_t *dst) {
  if (!st->init) {
    sha256_init (&st->ctx);
    sha256_update (&st->ctx, strlen (st->seed), (uint8_t*)st->seed);
    sha256_digest (&st->ctx, sizeof (st->state), st->state);
    st->init = true;
  }
  memcpy (dst, st->state, sizeof (st->state));
  
  sha256_update (&st->ctx, sizeof (st->state), st->state);
  sha256_digest (&st->ctx, sizeof (st->state), st->state);
}

void
fake_rand (const void *ctx, size_t len, uint8_t *dst) {
  const char *c = ctx;
  memset(dst, *c, len);
}

void
new_priv_key (priv_key *priv) {
  sym_key state;
  dumb_32_bytes (&_priv_key, state.data);
  
  *priv = sym_key_derive_priv_key (&state);
}
void
new_sym_key (sym_key *sym) {
  dumb_32_bytes (&_sym_key, sym->data);
}

void
randomize_priv_key (priv_key *priv) {
  getrandom (priv->data, sizeof (priv->data), 0);
  priv->data[0] &= 248;
  priv->data[31] &= 127;
  priv->data[31] |= 64;
}

void
randomize_sym_key (sym_key *sym) {
  getrandom (sym->data, sizeof (sym->data), 0);
}
