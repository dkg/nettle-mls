Implementation of Message Layer Security (MLS) key management in Nettle
=======================================================================

This is an attempt to provide a simple library that gives nettle the
ability to export an MLS keyshare.

Open decisions:

 * for non-volatile sessions, how should it serialize data?

 * what API is needed by a consumer?

The goal for this project is to be able to integrate with projects
written in C or C++.

If the end result is simple enough, it's conceivable that we could
contribute this code to nettle directly, so that it would not live as
an independent project.

I would like to be able to integrate it with notmuch.  And it would be
nice to be able to export bindings to higher-level languages like
python if possible.

Application-wise, I'm interested in using MLS to provide a way for
multiple mail user agents connected to the same e-mail account to
communicate with each other, to synchronize settings and other data.

Build environment
-----------------

As a simplifying initial assumption, this project targets building on
the debian platform.
