#include "tree.h"
#include <stdlib.h>
#include <string.h>

tree*
tree_new (const char *label) {
  static int n = 0;
  tree *t = calloc (sizeof (tree), 1);
  if (label)
    t->label = strdup (label);
  else
    asprintf (&t->label, "node %d", n++);
  return t;
}

void
tree_free (tree *t) {
  if (t == NULL)
    return;
  tree_free (t->left);
  tree_free (t->right);
  free (t->pub);
  free (t);
}

size_t
_tree_get_depth (tree *t) {
  size_t ret = 0;
  while (t) {
    ret += 1;
    t = t->left;
  }
  return ret;
}

tree *
_tree_add_new (tree *t, tree *n) {
  if (t == NULL) {
    return n;
  }

  if (1 << t->depth == t->children) {
    /* we are full.  make a new level and return it */
    tree *x = tree_new(NULL);
    x->left = t;
    t->parent = x;
    x->right = n;
    n->parent = x;
    x->depth = t->depth + 1;
    x->children = t->children + 1;
    return x;
  }

  /* otherwise, add the new node to the right-hand child */
  t->right = _tree_add_new (t->right, n);
  t->right->parent = t;
  t->children ++;
  return t;
}


tree *
tree_add_member (tree *t, const char *label, pub_key *pub, tree **new_ptr) {
  tree *n = tree_new (label);
  n->pub = malloc (sizeof (*pub));
  memcpy (n->pub, pub, sizeof(*pub));
  n->depth = 0;
  n->children = 1;

  if (new_ptr)
    *new_ptr = n;
  return _tree_add_new (tree_get_root (t), n);
}

static void
_fprint_tree (FILE *f, tree *t, int prefix) {
  for (int i = 0; i < prefix; i++)
    fputc (' ', f);
  fprintf (f, "[%10s]%s", t->label, t->pub? " ":"");
  if (t->pub)
    fprint_pub_key (f, "", t->pub);
  fputc ('\n', f);

  if (t->left)
    _fprint_tree (f, t->left, prefix + 1);
  if (t->right)
    _fprint_tree (f, t->right, prefix + 1);
}

void
fprint_tree (FILE *f, tree *t) {
  _fprint_tree(f, tree_get_root (t), 0);
}



void
fprint_tree_json (FILE *f, tree *t) {
  fprintf (f, "{\"pub\": \"");
  fprint_pub_key (f, NULL, t->pub);
  /* FIXME: t->label isn't safe to emit here, since it could be arbitrary values. */
  fprintf (f, "\", \"label\": \"%s\"", t->label);
  if (t->left) {
    fprintf (f, ",\"left\":");
    fprint_tree_json (f, t->left);
  }
  if (t->right) {
    fprintf (f, ",\"right\":");
    fprint_tree_json (f, t->right);
  }
  fprintf (f, "}");
}


