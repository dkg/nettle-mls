#include <stdio.h>
#include <stdlib.h>
#include "tree.h"
#include "primitives.h"

typedef struct member_t {
  tree *t;
  pub_key pub;
  priv_key priv;
  sym_key secret;
  const char * label;
} member;

void
init_member (member* m, const char *label) {
  m->t = NULL;
  randomize_sym_key (&m->secret);
  m->priv = sym_key_derive_priv_key (&m->secret);
  m->pub = priv_key_get_pub_key(&m->priv);
}
  

int main() {
  tree *t = NULL;
  int i;
  const char *labels[] = {
    "Alice",
    "Bob",
    "Charlie",
    "David"
  };
  const size_t n = sizeof(labels)/sizeof(*labels);
  member members[n];
  sym_key global;
  
  for (i = 0; i < n; i++) {
    fprintf (stdout, "----------Adding %s -----------\n", labels[i]);
    init_member (members+i, labels[i]);
    t = tree_add_member (t, labels[i], &members[i].pub, &members[i].t);
    global = members[i].secret;
    /* print public key for new leaf node, */
    for (tree *t2 = members[i].t; t2->parent; t2 = t2->parent) {
      /* print public and encrypted symmetric for each intermediate */
      global = sym_key_ratchet (&global);
      priv_key gpriv = sym_key_derive_priv_key (&global);

      t2->parent->pub = malloc (sizeof (pub_key));
      *t2->parent->pub = priv_key_get_pub_key (&gpriv);
    }
    fprint_tree (stdout, t);

    fprint_sym_key (stdout, "global", &global);
    fputc ('\n', stdout);
    fprint_tree_json (stdout, t);
    fputc ('\n', stdout);
  }

  fprintf (stdout, "---- state ----\n");
  for (i = 0; i < n; i++) {
    fprint_priv_key (stdout, labels[i], &members[i].priv);
    fputc ('\n', stdout);
    fprint_pub_key (stdout, labels[i], &members[i].pub);
    fputc ('\n', stdout);
    fprint_sym_key (stdout, labels[i], &members[i].secret);
    fputc ('\n', stdout);
  }

  return 0;
}
