CFLAGS :=  -Wall -pedantic -Werror -g -O3 $(shell pkg-config --cflags hogweed nettle) -Wno-gnu-folding-constant
LDFLAGS := $(shell pkg-config --libs hogweed nettle) -lgmp

OBJECTS = treekem art nettle-mls.so tree-test

all: $(OBJECTS)

nettle-mls.so: nettle-mls.c nettle-mls.h
	$(CC) $(CFLAGS) -shared -o $@ -lnettle -lhogweed $<

primitives.o: primitives.h
treekem.o: primitives.h
art.o: primitives.h
tree.o: primitives.h tree.h
tree-test.o: primitives.h tree.h

art: art.o primitives.o
treekem: treekem.o primitives.o tree.o
tree-test: tree.o tree-test.o primitives.o

clean:
	rm -f $(OBJECTS) *.o

.PHONY: clean all
