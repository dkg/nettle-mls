/* */

typedef struct mls_key_t mls_key;
typedef struct mls_private_key_t mls_private_key;
typedef struct mls_participant_t mls_participant;
typedef struct mls_conversation_t mls_conversation;


/* keys */

/* generating your own key */
mls_private_key *
mls_private_key_new();
void
mls_private_key_free(mls_private_key *mls);


/* pubkeys from AS or DS */
mls_key *
mls_key_init(const char *msg);
/* pubkeys from your own key */
mls_key *
mls_private_key_get_public(const mls_private_key*);
void
mls_key_free(mls_key *mls);





/* participants */

mls_participant *
mls_participant_init(const char *name, mls_key* );

void
mls_participant_free(mls_participant *mls);


/* conversations */


mls_conversation *
mls_conversation_init();

void
mls_conversation_free(mls_conversation *mls);
