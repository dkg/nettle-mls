#ifndef __TREE_H__
#define __TREE_H__

#include <stdbool.h>
#include "primitives.h"

typedef struct tree_t {
  size_t depth;
  size_t children;
  struct tree_t *parent, *left, *right;
  char * label;
  pub_key *pub;
} tree;

tree*
tree_new (const char *label);

void
tree_free (tree *t);

inline bool
tree_is_empty (tree *t) {
  return t->left == NULL && t->right == NULL &&
    t->pub == NULL;
}

inline bool
tree_is_leaf (tree *t) {
  return t->pub != NULL;
}

inline bool
tree_is_root (tree *t) {
  return t->parent == NULL;
}

inline tree *
tree_get_root (tree *t) {
  while (t && t->parent)
    t = t->parent;
  return t;
}

tree *
tree_add_member (tree *t, const char *label, pub_key *pub, tree **new_ptr);

void
fprint_tree (FILE *f, tree *t);

void
fprint_tree_json (FILE *f, tree *t);

void
fprint_tree_dot (FILE *f, tree *t);

#endif /* __TREE_H__ */
