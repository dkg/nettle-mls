#ifndef __PRIMITVES_H__
#define __PRIMITVES_H__

#define _GNU_SOURCE
#include <stdio.h>
#include <nettle/curve25519.h>

typedef struct priv_key_t {
  uint8_t data[CURVE25519_SIZE];
} priv_key;

typedef struct pub_key_t {
  uint8_t data[CURVE25519_SIZE];
} pub_key;

typedef struct sym_key_t {
  uint8_t data[CURVE25519_SIZE];
} sym_key;  


void fprint_pub_key (FILE* f, const char * label, const pub_key *pub);
void fprint_priv_key (FILE* f, const char * label, const priv_key *priv);
void fprint_sym_key (FILE* f, const char * label, const sym_key *sym);

pub_key priv_key_get_pub_key (const priv_key *priv);

sym_key kx (const priv_key *priv, const pub_key* pub);

priv_key sym_key_derive_priv_key (const sym_key *sym);
sym_key sym_key_ratchet (const sym_key *sym);
void fake_rand (const void *ctx, size_t len, uint8_t *dst);

void new_priv_key (priv_key *priv);
void new_sym_key (sym_key *sym);

void randomize_sym_key (sym_key *sym);
void randomize_priv_key (priv_key *priv);

#endif /* __PRIMITIVES_H__ */
