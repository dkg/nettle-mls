#include <stdio.h>
#include "primitives.h"

int main() {

  int i;
  const char *labels[] = {
    "Alice",
    "Bob",
    "Charlie",
    "David"
  };
  const size_t n = sizeof(labels)/sizeof(*labels);
  pub_key pub[n];
  priv_key priv[n];
  sym_key secret[n];

  priv_key priv2[n/2];
  pub_key pub2[n/2];
  sym_key secret2[n/2];
  
  for (i = 0; i < n; i++) {
    fake_rand (labels[i], sizeof(priv[i].data), priv[i].data);
    pub[i] = priv_key_get_pub_key(priv+i);
  }
  
  /* this is ART, not TreeKEM actually */

  secret[0] = kx (priv + 0, pub + 1);
  secret[1] = kx (priv + 1, pub + 0);

  secret[2] = kx (priv + 2, pub + 3);
  secret[3] = kx (priv + 3, pub + 2);

  for (i = 0; i < n/2; i++) {
    priv2[i] = sym_key_derive_priv_key (secret + i*2);
    pub2[i] = priv_key_get_pub_key (priv2+i);
  }

  secret2[0] = kx (priv2 + 0, pub2 + 1);
  secret2[1] = kx (priv2 + 1, pub2 + 0);

  
  for (i = 0; i < n; i++) {
    fprint_priv_key (stdout, labels[i], priv+i);
    fputc('\n', stdout);
    fprint_pub_key (stdout, labels[i], pub+i);
    fputc('\n', stdout);
    fprint_sym_key (stdout, labels[i], secret+i);
    fputc('\n', stdout);
  }

  for (i = 0; i < n/2; i++) {
    char buf[100];
    sprintf (buf, "l2 %d", i);
    fprint_priv_key (stdout, buf, priv2+i);
    fputc('\n', stdout);
    fprint_pub_key (stdout, buf, pub2+i);
    fputc('\n', stdout);
    fprint_sym_key (stdout, buf, secret2+i);
    fputc('\n', stdout);
  }

  return 0;
}
